<?php
/*
Template Name: Full Width Page
*/
get_header(); ?>
<main id="content" class="grid cx" role="main">
    <div class="grid cx w100">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class('w100'); ?>  >

            <div class="entry-content">
                <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
                <?php the_content(); ?>
                <div class="entry-links"><?php wp_link_pages(); ?></div>
            </div>
        </article>
        <?php if ( comments_open() && ! post_password_required() ) { comments_template( '', true ); } ?>
        <?php endwhile; endif; ?>
    </div>


</main>
<?php get_footer(); ?>