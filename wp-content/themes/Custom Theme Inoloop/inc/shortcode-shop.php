<?php
function productos_ino($atts){
    $param = shortcode_atts( array (
        'cantidad' => '-1',
        'tax' => 'product_cat',
        'tax_name' => 'term',
        'titulo' => 'true',
        'precio' => 'true',
        'boton' => 'true',        
        'columnas' => 4,
        ), $atts );

        $args = array(
            'post_type'             => 'product',
            'post_status'           => 'publish',
            'ignore_sticky_posts'   => 1,
            'posts_per_page'        => $param['cantidad'],
            'tax_query'             => array(
                array(
                    'taxonomy'      => $param['tax'],
                    'field'         => 'slug', 
                    'terms'         => $param['tax_name'],
                ),
            )
        );
    global $post;
    $query = new WP_Query( $args );
    $html='<div class="woocommerce"> <ul class="products columns-'.$param["columnas"].'">';
   
   // The Loop
    while ( $query->have_posts() ) {
        $query->the_post();
        $html.='<li class="product ">
        <a href="'.get_permalink().'"><img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ),"woocommerce_thumbnail" )[0].'" alt="">';
        $html.='<div class="info-producto">';
        if($param["titulo"]!="false"){
            $html.='<h2>'.get_the_title().'</h2>';                
        } 
        if($param["precio"]!="false" && get_post_meta( get_the_ID(), '_price', true )!=null){
            $html.='<div class="price">S/ '. number_format(get_post_meta( get_the_ID(), "_price", true ), 2).'</div>';
        } 
        $html.='</a>';
        if($param["boton"]!="false"){
            $html.='<a href="" class="button">Añadir al carrito </a>';
        } 
        $html.='</div></li>';
    }
    $html.='</ul></div><style>
        div.woocommerce ul.products li:nth-child('.$param["columnas"].'n){
            margin:0;
        }
    </style>'; 
    wp_reset_postdata();

return $html;
}

add_shortcode('productos_ino', 'productos_ino');


function productos_ino_car($atts){
    $param = shortcode_atts( array (
        'cantidad' => '-1',
        'tax' => 'product_cat',
        'tax_name' => 'term',
        'titulo' => 'true',
        'precio' => 'true',
        'boton' => 'true',        
        'columnas' => 4,
        ), $atts );

        $args = array(
            'post_type'             => 'product',
            'post_status'           => 'publish',
            'ignore_sticky_posts'   => 1,
            'posts_per_page'        => $param['cantidad'],
            'tax_query'             => array(
                array(
                    'taxonomy'      => $param['tax'],
                    'field'         => 'slug', 
                    'terms'         => $param['tax_name'],
                ),
            )
        );
    global $post;
    $query = new WP_Query( $args );
    $html='<div class="woocommerce"> <ul class="products products-slick">';
   
   // The Loop
    while ( $query->have_posts() ) {
        $query->the_post();
        $html.='<li class="product ">
        <a href="'.get_permalink().'"><img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ),"woocommerce_thumbnail" )[0].'" alt="">';
        $html.='<div class="info-producto">';
        if($param["titulo"]!="false"){
            $html.='<h2>'.get_the_title().'</h2>';                
        } 
        if($param["precio"]!="false" && get_post_meta( get_the_ID(), '_price', true )!=null){
            $html.='<div class="price">S/ '. number_format(get_post_meta( get_the_ID(), "_price", true ), 2).'</div>';
        } 
        $html.='</a>';
        if($param["boton"]!="false"){
            $html.='<a href="" class="button">Añadir al carrito </a>';
        } 
        $html.='</div></li>';
    }
    $html.='</ul></div><style>
        div.woocommerce ul.products li:nth-child('.$param["columnas"].'n){
            margin:0;
        }
    </style>'; 
    wp_reset_postdata();

return $html;
}

add_shortcode('productos_ino_car', 'productos_ino_car');


function productos_ino_c($atts){
    $param = shortcode_atts( array (
        'cantidad' => '-1',
        'tax' => 'product_cat',
        'tax_name' => 'term',
        'titulo' => 'true',
        'precio' => 'true',
        'boton' => 'true',        
        'columnas' => 4,
        ), $atts );

        $args = array(
            'post_type'             => 'product',
            'post_status'           => 'publish',
            'ignore_sticky_posts'   => 1,
            'posts_per_page'        => $param['cantidad'],
            'tax_query'             => array(
                array(
                    'taxonomy'      => $param['tax'],
                    'field'         => 'slug', 
                    'terms'         => $param['tax_name'],
                ),
            )
        );
    global $post;
    $query = new WP_Query( $args );
    $html='<div class="woocommerce"> <ul class="products columns-'.$param["columnas"].'">';
   
   // The Loop
    while ( $query->have_posts() ) {
        $query->the_post();
        $html.='<li class="product ">
        <a href="'.get_permalink().'"><img src="'.wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ),"woocommerce_thumbnail" )[0].'" alt="">';
        $html.='<div class="info-producto">';
        if($param["titulo"]!="false"){
            $html.='<h2>'.get_the_title().'</h2>';                
        } 
        if($param["precio"]!="false" && get_post_meta( get_the_ID(), '_price', true )!=null){
            $html.='<div class="price">S/ '. number_format(get_post_meta( get_the_ID(), "_price", true ), 2).'</div>';
        } 
        $html.='</a>';
        if($param["boton"]!="false"){
            $html.='<a href="" class="button">Añadir al carrito </a>';
        } 
        $html.='</div></li>';
    }
    $html.='</ul></div><style>
        div.woocommerce ul.products li:nth-child('.$param["columnas"].'n){
            margin:0;
        }
    </style>'; 
    wp_reset_postdata();

return $html;
}

add_shortcode('productos_ino_c', 'productos_ino_c');



function productos_destacados(){
    global $post;
    $args = array(
        'post_type'             => 'product',
        'post_status'           => 'publish',
        'ignore_sticky_posts'   => 1,
        'posts_per_page'        => -1,
        'orderby'     => 'date',
        'order'       => 'DESC' ,
        'tax_query'  => array(
            array(
                'taxonomy' => 'product_visibility',
                'field'    => 'name',
                'terms'     => 'featured'
            )
        )
     );

$query = new WP_Query( $args );
// The Loop


while ( $query->have_posts() ) {
    $query->the_post();?>
<li class="product ">
    <a href="<?php echo get_permalink(); ?>">
        <?php
                echo the_post_thumbnail('woocommerce_thumbnail');
            ?>
        <div class="producto-info">
            <h2>
                <?php echo get_the_title(); ?>
            </h2>
            <div class="price">
                <?php
                    if(get_post_meta( get_the_ID(), '_price', true )!=null){

                    echo "S/".number_format(get_post_meta( get_the_ID(), '_price', true ), 2);

                }
                    ?>
            </div>
            <a href="" class="button">
                Añadir al carrito
            </a>
        </div>
    </a>
</li>   

<?php }

}
add_shortcode('productos_destacados', 'productos_destacados');

function categories_shop(){
    global $post;
    $args = array(
        'taxonomy'   => "product_cat",
        'exclude' => "uncategorized"

    );
    $product_categories = get_terms($args);
    $html='<div class="categories grid cx"><a class="category grid cx cy" href="'.get_permalink( wc_get_page_id( 'shop' ) )
    .'"><h2>Categorias</h2></a>';
    foreach( $product_categories as $cat ){
        $html.='<a class="category grid cx" href="'. get_term_link( $cat ). '">
            <img src="'.wp_get_attachment_url(get_term_meta( $cat->term_id, "thumbnail_id", true )).'" alt="categories" />
            <div class="grid cx cy tac"> '. $cat->name.' </div>
        </a>';
        } 
     $html.='</div>';

    return $html;
    }
add_shortcode('categories_shop', 'categories_shop');
?>