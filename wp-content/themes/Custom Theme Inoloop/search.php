<?php
/**

 *
 * @link https://www.inoloop.com/
 * @since 1.0.0
 */

get_header();
?>

<div>
    <main class="grid cx woocommerce">
        <div class="sidebar">
            <?php if ( is_active_sidebar( 'primary-widget-area' ) ) : ?>
            <?php dynamic_sidebar( 'primary-widget-area' ); ?>
            <?php endif; ?>
        </div>
        <div class="content">
            <h1>Resultados para "<?php the_search_query(); ?>"</h1>
            <ul class="products columns-5">
                <?php if (have_posts()) : 
                    ?>
                <?php global $wp_query; ?>
                <p>Se encontraron <?php echo  $wp_query->found_posts ?> productos</p>
                <?php while (have_posts()) : the_post(); ?>

                <li class="product">
                    <a href="<?php echo get_permalink(); ?>">
                        <?php the_post_thumbnail('woocommerce_thumbnail'); ?>
                        <h2 class="woocommerce-loop-product__title"><?php the_title(); ?></h2>

                    </a>
                </li>

                <?php endwhile; ?>
                <?php 
                	else :

                       ?>

                <p>No se encontraron resultados, prueba con otras palabras</p>
                <?php
        
                    endif;
                ?>
            </ul>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php
get_footer();