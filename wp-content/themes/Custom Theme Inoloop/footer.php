<?php
/**
 */

?>

</div><!-- #content -->

<footer class="grid cx">
    <div class="w100 grid cy cxa" style="padding:20px">
    <a href="/"><img src="/wp-content/themes/Custom Theme Inoloop/img/logo-footer.png" alt="logo" class="logo"></a>    
            <div>
                <h3>SÍGUENOS</h3>
                <a href="https://www.facebook.com/CortadordebotellasVidrillete/" target="_blank" class="icon-facebook-circulo"></a>
                <a href="https://web.whatsapp.com/send?phone=51961787174&text=Hola%20estoy%20interesado%20en%20Vidrillete" target="_blank" class="icon-whatsapp-circulo"></a>

            </div>
    </div>
    <div class="w100 cpr">
        COPYRIGHT © 2020 VIDRILLETE | DESARROLLADO POR <a href="https://inoloop.com/">INOLOOP</a>
    </div>
</footer>

</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript" src="/wp-content/themes/Custom Theme Inoloop/js/slick.js"></script>
<script>
jQuery(document).ready(function() {
    var screen = jQuery(window)
    jQuery("#toggle-m").on("click", function() {
        jQuery(this).toggleClass("active");
        jQuery(this).parents("div").siblings(".mob").toggle();
    });
    jQuery("#toggleb").on("click", function() {
        jQuery(this).toggleClass("active");
        jQuery(this).siblings("div").toggle();
    });
    if (screen.width() < 1200) {
        jQuery("#content .content").on("click", function() {
            jQuery(this).siblings("div").children(".w100").hide();
        });
    }

    /*SLICK*/
    jQuery('.products-slick').slick({

        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        responsive: [{
                breakpoint: 1080,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 780,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]


    });
    /*SLICK*/
    
    /*MENU*/
    //AGREGAMOS FLECHAS A LOS MENU QUE TIENEN SUBMENU
    jQuery(".menu-item-has-children").each(function() {
      //jQuery( this ).find(">a").append( "<span class='sub-menu-arrow'><span>");
      jQuery( "<span class='sub-menu-arrow icon-f-abajo'></span>").insertAfter( jQuery(this).find(">a"));
    });
    //CUANDO HACEN CLICK EN LA FLECHA, SE AGREGA LA CLASE QUE MUESTRA O ESCONDE EL SUBMENU
    jQuery(document).on("click", ".menu-item-has-children" , function() {
        jQuery(this).closest('li.menu-item').toggleClass("parent-open-sub-menu");
    });
    //ESCONDEMOS EL LOGO EN EL HEADER DE MOVILES
    jQuery( ".mob ul.menu>li.menu-item" ).has("img").hide();
    /*MENU*/

});
</script>
</body>

</html>