<form action="/" method="get" class="grid cx cy">
    <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" />
    <input type="image" class="search-bt" alt="Search"
        src="<?php echo get_template_directory_uri()."/images/search.png"; ?>" />
</form>