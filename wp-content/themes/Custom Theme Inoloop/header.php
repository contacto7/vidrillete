<?php
/**

 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?php bloginfo('name'); ?> | <?php wp_title(); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
    <link href="/wp-content/themes/Custom Theme Inoloop/css/slick.css" rel="stylesheet">
    <link href="/wp-content/themes/Custom Theme Inoloop/css/slick-theme.css" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Ads: 586556498 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-586556498"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-586556498');
    </script>
        <!-- Event snippet for Website lead conversion page -->
    <script>
      gtag('event', 'conversion', {'send_to': 'AW-586556498/52pSCKG_-d4BENLI2JcC'});
    </script>

        <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <nav class="grid cx">
        <div class="up w100 grid cy cx">
            Envios Internacionales
        </div>
        <div class="down w100 grid cx">
            <div class="desk estrecho grid cxb">
               <a href="/"><img src="/wp-content/themes/Custom Theme Inoloop/img/logo.png" alt="logo" class="logo"></a> 
            <?php
                wp_nav_menu( array( 
                'theme_location' => 'header-menu', 
                'container_class' => 'navigation' ) ); 
            ?>
         

            </div>
            <div class="toggle-m w100 cxb cy">     
            <a href="/"><img src="/wp-content/themes/Custom Theme Inoloop/img/logo-footer.png" alt="logo" class="logo"></a> 
            <button id="toggle-m" class="grid cx">
                <span></span>
                <span></span>
                <span></span>
            </button>
            </div>
            <div class="mob">
            <?php
                wp_nav_menu( array( 
                    'theme_location' => 'header-menu', 
                    'container_class' => 'navigation' 
                ) ); 
            ?>
            </div>
        </div>

    </nav>
    <div>
        <div id="content" class="site-content">